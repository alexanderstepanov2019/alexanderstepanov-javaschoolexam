package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty() || hasIlligalSymbols(statement)) {
            return null;
        }
        List<String> postfix = parse(statement);
        if (postfix != null) {
            return calc(postfix);
        }
        return null;
    }
    private static List<String> tokenizer(String expr) {
        List<String> tokens = new ArrayList<>();
        if (expr != null && !expr.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            boolean digitRecord = false;
            boolean hasDot = false;
            for (int i = 0; i < expr.length(); i++) {
                char symbol = expr.charAt(i);
                switch (symbol) {
                    case '.': {
                        if (digitRecord && !hasDot) {
                            hasDot = true;
                        } else {
                            return null;
                        }
                    }
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9': {
                        digitRecord = true;
                        sb.append(symbol);
                        break;
                    }
                    case '+':
                    case '-':
                    case '*':
                    case '/':
                    case '(':
                    case ')': {
                        if (digitRecord) {
                            tokens.add(sb.toString());
                            digitRecord = false;
                        }
                        sb.delete(0, sb.length());
                        tokens.add(symbol + "");
                        break;
                    }
                    case ' ': break;
                    default: return null;
                }
            }
            if (sb.length() > 0) {
                tokens.add(sb.toString());
            }
        }
        return tokens;
    }

    private boolean isOperator(String statement) {
        return "+-*/".contains(statement);
    }

    private boolean isDelimiter(String curr) {
        return "+-*/()".contains(curr);
    }

    private static int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    private List<String> parse(String infix) {
        List<String> postfix = new ArrayList<String>();
        Stack<String> stack = new Stack<>();
        List<String> tokenizer = tokenizer(infix);
        String prev = "";
        String curr = "";
        if (tokenizer == null) {
            return null;
        }
        for (int i = 0; i < tokenizer.size(); i++) {
            curr = tokenizer.get(i);
            if (i == tokenizer.size() - 1 && isOperator(curr)) {
                return null;
            }
            if (isDelimiter(curr)) {
                if (curr.equals("(")) stack.push(curr);
                else if (curr.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        postfix.add(stack.pop());
                        if (stack.isEmpty()) {
                            return null;
                        }
                    }
                    stack.pop();
                } else {
                    if ((curr.equals("-") && !isOperator(prev)) && (prev.equals("") || (isDelimiter(prev) && !prev.equals(")")))) {
                        curr = "u";
                    } else if (isOperator(curr) && isOperator(prev)) {
                        return null;
                    } else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }

                    }
                    stack.push(curr);
                }

            } else {
                postfix.add(curr);
            }
            prev = curr;
        }

        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) postfix.add(stack.pop());
            else {
                return null;
            }
        }
        return postfix;
    }

    private String calc(List<String> postfix) {
        String result = "";
        Stack<Double> stack = new Stack<>();
        for (String elem : postfix) {
            if (elem.equals("u")) {
                double digit = stack.pop();
                stack.push((-1) * digit);

            } else if (elem.equals("+")) {
                double digit1 = stack.pop();
                double digit2 = stack.pop();
                stack.push(digit1 + digit2);
            } else if (elem.equals("-")) {
                double digit1 = stack.pop();
                double digit2 = stack.pop();
                stack.push(digit2 - digit1);
            } else if (elem.equals("*")) {
                double digit1 = stack.pop();
                double digit2 = stack.pop();
                stack.push(digit1 * digit2);
            } else if (elem.equals("/")) {
                double digit1 = stack.pop();
                double digit2 = stack.pop();
                if (digit1 == 0) {
                    return null;
                }
                stack.push(digit2 / digit1);
            } else {
                try {
                    stack.push(Double.parseDouble(elem));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }

        BigDecimal resultBd = new BigDecimal(stack.pop())
                .setScale(4, RoundingMode.HALF_UP);

        result = resultBd.stripTrailingZeros()
                .toPlainString();
        return result;
    }

    private String calcBd(List<String> postfix) {
        String result = "";
        Stack<BigDecimal> stack = new Stack<>();
        for (String elem : postfix) {
            if (elem.equals("u")) {
                BigDecimal digit = stack.pop();
                stack.push(digit.negate());

            } else if (elem.equals("+")) {
                BigDecimal digit1 = stack.pop();
                BigDecimal digit2 = stack.pop();
                stack.push(digit2.add(digit1));
            } else if (elem.equals("-")) {
                BigDecimal digit1 = stack.pop();
                BigDecimal digit2 = stack.pop();
                stack.push(digit2.subtract(digit1));
            } else if (elem.equals("*")) {
                BigDecimal digit1 = stack.pop();
                BigDecimal digit2 = stack.pop();
                stack.push(digit1.multiply(digit2));
            } else if (elem.equals("/")) {
                BigDecimal digit1 = stack.pop();
                BigDecimal digit2 = stack.pop();
                stack.push(digit2.divide(digit1, RoundingMode.CEILING));
            } else {
                try {
                    stack.push(new BigDecimal(elem));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        return stack.pop().toString();
    }


    private boolean hasIlligalSymbols(String expr) {
        String check = expr
                .replaceAll("[0-9.]+", "")
                .replaceAll("\\+", "")
                .replaceAll("\\-", "")
                .replaceAll("\\*", "")
                .replaceAll("\\/", "")
                .replaceAll("\\(", "")
                .replaceAll("\\)", "");

        return !check.isEmpty();
    }

}
