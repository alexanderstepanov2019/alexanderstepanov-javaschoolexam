package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || containsNull(inputNumbers)) {
            throw new CannotBuildPyramidException();
        }
        try {
            int min = Integer.MAX_VALUE;
            for (Integer elem : inputNumbers) {
                if (elem < min) {
                    min = elem;
                }
            }

            inputNumbers.sort(Comparator.comparingInt(Integer::intValue));


            int size = calcRows(inputNumbers.size());
            if (size < 0) {
                throw new CannotBuildPyramidException();
            }
            int currentIndex = 0;
            int rowSize = size + 2, columnSize = size * 2 + 3;
            int center = columnSize / 2;
            int[][] result = new int[rowSize][columnSize];
            for (int i = 0, offset = 0, count = 1; i < result.length; i++, offset++, count++) {
                int start = center - offset;
                for (int j = 0; j < count * 2; j += 2) {
                    result[i][start + j] = inputNumbers.get(currentIndex++);
                }

            }
            return result;
        } catch (OutOfMemoryError memoryError) {
            throw new CannotBuildPyramidException();
        }

    }

    private boolean containsNull(List<Integer> list) {
        for (Integer elem : list) {
            if (elem == null) {
                return true;
            }
        }
        return false;
    }

    private int calcRows(int arraySize) {
        int col = 1, row = 2;
        if (arraySize < 3)
            return 0;
        while (col < arraySize) {
            col += row;
            row++;
        }
        if (col != arraySize) {
            return -1;
        } else {
            return row - 3;
        }
    }


}
