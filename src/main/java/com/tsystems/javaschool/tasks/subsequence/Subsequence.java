package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("ggg");
        } else if (x.isEmpty()) {
            return true;

        } else if (y.isEmpty() || x.size() > y.size()) {
            return false;
        } else {

            int indexX = 0, indexY = 0;
            while (indexX < x.size()) {
                while (!x.get(indexX).equals(y.get(indexY))) {
                    indexY++;
                    if (indexY >= y.size()) {
                        return false;
                    }
                }
                if (x.get(indexX).equals(y.get(indexY))) {
                    indexX++;
                    indexY++;
                    if (indexX == x.size())
                        return true;
                    if (indexY >= y.size())
                        return false;
                }
            }
        }
        return false;
    }
}
